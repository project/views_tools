<?php

namespace Drupal\Tests\views_tools\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests uninstalling views_tools doesn't remove other module's views_tools.
 *
 * @group views_tools
 */
class ViewsToolsUninstallTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['views_tools'];

  /**
   * Tests views_tools uninstall.
   */
  public function testViewsToolsUninstall() {
    \Drupal::service('module_installer')->uninstall(['views_tools']);
    $this->drupalGet('/admin/structure/views-tools');
    $this->assertSession()->statusCodeEquals(404);
  }

}
